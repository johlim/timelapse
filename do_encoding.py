from os import system
from time import sleep
from datetime import datetime
import glob
import os
import sys

def convert_jpg_to_mp4(curdir):
	path_dir = '*.jpg'

	file_list = os.listdir('/home/pi/timelapse')
	file_list = glob.glob(path_dir)
	file_list.sort()
	file = file_list[0]
	print file 
	start = int(file.split(".jpg")[0])
	print start
	filename = datetime.today().strftime("%Y%m%d%H%M%S")
	syscmd =  'convert -adjoin -delay 10 -loop 0 *.jpg '+filename +".gif"
	#syscnd = system('convert -delay 10 -loop 0 *.jpg '+filename)
	#echo $DATE
	syscmd = "avconv -r 0.1 -start_number " + str(start) +" -i %04d.jpg -r 1 -vcodec libx264 -crf 20 -g 15 -vf scale=1280:720 tl" + filename+".mp4"
	#print syscmd
	system(syscmd)

def main(arg):
	convert_jpg_to_mp4(arg)


if __name__ == "__main__":
    try :
        main(sys.argv[0])
    finally :
        pass
