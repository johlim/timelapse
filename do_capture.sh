#!/bin/bash
#gst-launch-1.0 multifilesrc location=image%04d.jpg index=1 caps="image/jpeg,framerate=24/1" ! jpegdec ! omxh264enc ! avimux ! filesink location=timelapse.avi


DATE=$(date +"%H%M")
#DATE=$(date +"%Y%m%d%H%M")
#echo $DATE
raspistill -n -vf -hf -w 1280 -h 720 -o /home/pi/timelapse/$DATE.jpg -vs -awb auto -fli auto -drc high

